package net.librec.recommender.content;

/**
 * Created by himanabdollahpouri on 2/19/17.
 */

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.*;

import com.google.common.collect.*;
import net.librec.common.LibrecException;
import net.librec.math.structure.*;
import net.librec.recommender.AbstractRecommender;

public class NaiveBayesRecommender extends AbstractRecommender {
    private static final int BSIZE = 1024 * 1024;
    Map<Integer, List<Integer>> item_likes_dislikes = new HashMap<>();
    protected SparseMatrix m_featureMatrix;
    Map<Integer, Table<Integer, Integer, Double>> user_feature_like_disLike = new HashMap<>();
    protected double m_threshold;
    // user/feature {raw id, inner id} map
    private BiMap<String, Integer> itemIds, featureIds;
    Table<Integer, Integer, Integer> contentTable = HashBasedTable.create();

    /**
     * Set up code expects a file name under the parameter dfs.content.path. It loads
     * comma-separated feature data and stores a row at a time in the contentTable.
     * After the data is loaded, it is converted into a SparseMatrix and stored in
     * m_featureMatrix. This code borrows heavily from the implementation in
     * net.librec.data.convertor.TextDataConvertor.
     **/
    @Override
    protected void setup() throws LibrecException {
        super.setup();

        String contentPath = conf.get("dfs.content.path");
        HashBiMap<String, Integer> itemIds = HashBiMap.create();
        HashBiMap<String, Integer> featureIds = HashBiMap.create();
        String prefix = "/Users/himanabdollahpouri/Downloads/librec-2.0.0/data/";
        int rowCount = 0;
        int maxFeature = -1;

        try {
            FileInputStream fileInputStream = new FileInputStream(prefix.concat(contentPath));
            FileChannel fileRead = fileInputStream.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(BSIZE);
            int len;
            String bufferLine = new String();
            byte[] bytes = new byte[BSIZE];
            while ((len = fileRead.read(buffer)) != -1) {
                buffer.flip();
                buffer.get(bytes, 0, len);
                bufferLine = bufferLine.concat(new String(bytes, 0, len));
                // String spl = System.getProperty("line.separator");
                String[] bufferData = bufferLine.split(System.getProperty("line.separator") + "+");
                boolean isComplete = bufferLine.endsWith(System.getProperty("line.separator"));
                int loopLength = isComplete ? bufferData.length : bufferData.length - 1;
                for (int i = 0; i < loopLength; i++) {
                    String line = new String(bufferData[i]);
                    String[] data = line.trim().split("[ \t,]+");

                    String item = data[0];
                    // inner id starting from 0
                    int row = itemIds.containsKey(item) ? itemIds.get(item) : itemIds.size();
                    itemIds.put(item, row);

                    for (int j = 1; j < data.length; j++) {
                        String feature = data[j];
                        int col = featureIds.containsKey(feature) ? featureIds.get(feature) : featureIds.size();
                        featureIds.put(feature, col);

                        contentTable.put(row, col, 1);
                    }

                }
            }
        } catch (IOException e) {
            LOG.error("Error reading file: " + contentPath + e);
            throw (new LibrecException(e));
        }

        m_featureMatrix = new SparseMatrix(itemIds.size(), featureIds.size(), contentTable);
        //double k = m_featureMatrix.get(4584, 4);
        LOG.info("Loaded item features from " + contentPath);
    }

    @Override
    protected void trainModel() throws LibrecException {

        List<Integer> userIList = trainMatrix.rows();
        for (Integer userIdx : userIList) {
            Set<Integer> userRatedItems = trainMatrix.getColumnsSet(userIdx);
            Table<Integer, Integer, Double> featureLike_Dislike = HashBasedTable.create();
            for (Integer item : userRatedItems) {
                Set<Integer> features = m_featureMatrix.getColumnsSet(item);

                for (Integer feature : features) {
                    if (trainMatrix.get(userIdx, item) > 3) {
                        if (featureLike_Dislike.contains(feature, 0)) {
                            double value = featureLike_Dislike.get(feature, 0);
                            value += 1;
                            featureLike_Dislike.put(feature, 0, value);
                        } else {
                            featureLike_Dislike.put(feature, 0, 1d);
                            featureLike_Dislike.put(feature, 1, 0d);
                        }
                    } else {
                        if (featureLike_Dislike.contains(feature, 1)) {
                            double value = featureLike_Dislike.get(feature, 1);
                            value += 1;
                            featureLike_Dislike.put(feature, 1, value);
                        } else {
                            featureLike_Dislike.put(feature, 1, 1d);
                            featureLike_Dislike.put(feature, 0, 0d);
                        }
                    }

                }

            }
            user_feature_like_disLike.put(userIdx, featureLike_Dislike);
        }
    }

    @Override
    protected double predict(int userIdx, int itemIdx) throws LibrecException {
        Table<Integer, Integer, Double> userFeatureLikeDislikes = user_feature_like_disLike.get(userIdx);
        Set<Integer> userRatedItems = trainMatrix.getColumnsSet(userIdx);
        int numLikes = 0;
        int numDislikes = 0;
        for (Integer item : userRatedItems) {
            if (trainMatrix.get(userIdx, item) > 3)
                numLikes += 1;
            else numDislikes += 1;
        }

        // Here we calculate the prior probability (i.e. P(Like) and also P(Dislike)
        double pLike = (double) (numLikes) / (numDislikes + numLikes);
        double pDislike = (double) (numDislikes) / (numDislikes + numLikes);

        //Now we should calculate the probability of each features in the item, given Like or Dislike
        //For the probability of each feature given Like, we look at the
        //userFeatureLikeDislikes.get(feature,0) divided by sum of values of Likes features
        //and for the probability of each feature given DisLike, we look at the
        //userFeatureLikeDislikes.get(feature,1) divided by sum of values of dislikes feature

        //First let's get the features of item itemIdx
        Set<Integer> features = m_featureMatrix.getColumnsSet(itemIdx);

        //Now let's for each feature compute the probability of P(feature|Like) and
        // calculate the multiplication of them
        // First we need the sum of values of the Likes column (i.e. userFeatureLikeDislikes.column(0)
        //and also the sum of values of the DisLikes column (i.e. userFeatureLikeDislikes.column(1)
        //We need this two values for the denominator of the P(fi|Like)=#(fi,like)/Sum(likes) and also
        //P(fi|disLike)=#(fi,dislike)/Sum(dislikes)

        int sumOfLikes = 0;
        int sumOfDislikes = 0;
        sumOfLikes = sum(userFeatureLikeDislikes.column(0).values());
        sumOfDislikes = sum(userFeatureLikeDislikes.column(1).values());

        //Now let's for each feature compute the probability of P(feature|Like) and
        // calculate the multiplication of them
        double multOfProbabilities_Like = 1;
        double multOfProbabilities_Dislike = 1;
        for (Integer feature : features) {
            if (userFeatureLikeDislikes.containsRow(feature)) {
                multOfProbabilities_Like *= (double) (userFeatureLikeDislikes.get(feature, 0) + 1) / sumOfLikes;
                multOfProbabilities_Dislike *= (double) (userFeatureLikeDislikes.get(feature, 1) + 1) / sumOfDislikes;
            } else {
                multOfProbabilities_Like *= 1;
                multOfProbabilities_Dislike *=1;
            }
        }
        return Math.log(((double) (pLike * multOfProbabilities_Like) / pDislike * multOfProbabilities_Dislike));
    }

    //***************************************************************************
    private int sum(Collection<Double> values) {
        int sum = 0;
        for (double value : values)
            sum += value;
        return sum;
    }

}