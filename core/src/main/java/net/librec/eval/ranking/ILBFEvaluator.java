package net.librec.eval.ranking;

import net.librec.eval.AbstractRecommenderEvaluator;
import net.librec.math.structure.DenseMatrix;
import net.librec.math.structure.DenseVector;
import net.librec.math.structure.SparseMatrix;
import net.librec.math.structure.SymmMatrix;
import net.librec.recommender.item.ItemEntry;
import net.librec.recommender.item.RecommendedList;

import java.util.List;
import java.util.Random;

/**
 * Created by himanabdollahpouri on 3/16/17.
 */
public class ILBFEvaluator extends AbstractRecommenderEvaluator {
    @Override
    public double evaluate(SparseMatrix trainMatrix, SparseMatrix testMatrix, RecommendedList recommendedList) {
        double totalLongTailPercentage = 0.0;
        int numUsers = testMatrix.numRows();
        int nonZeroNumUsers = 0;
        int longTailCount = 0;
        if (similarities.containsKey("item")) {
            DenseVector longTailVector = similarities.get("item").getItemClusterVector();

            for (int userID = 0; userID < numUsers; userID++) {
                longTailCount=0;
                List<ItemEntry<Integer, Double>> recommendArrayListByUser = recommendedList.getItemIdxListByUserIdx(userID);
                if (recommendArrayListByUser.size() > 0) {
                    // calculate the sum of similarities for each pair of items per user
                    int topK = this.topN <= recommendArrayListByUser.size() ? this.topN : recommendArrayListByUser.size();
                    for (int i = 0; i < topK; i++) {
                        if (longTailVector.get(i)==1)
                            longTailCount++;
                    }
                    totalLongTailPercentage += longTailCount / (topK);
                    nonZeroNumUsers++;
                }
            }
        }

        return nonZeroNumUsers > 0 ? totalLongTailPercentage / nonZeroNumUsers : 0.0d;
    }

}

